# Serializers Benchmark
A JMH based comparison between most common serializing standards. 

This micro benchmark suite was originally developed in Java/Scala with the support of the library [JMH](http://openjdk.java.net/projects/code-tools/jmh/)

All the algorithms tested so far has been implemented in a pluggable fancy. It is trivial to plug new algorithms or testing models.

## Plugged Algorithms

[Procol Buffer](https://github.com/google/protobuf) - Google's standard data interchange format.
[Colfer](https://github.com/pascaldekloe/colfer) - A serialization standard specifically designed for high speed and low size.
[Thrift](https://thrift.apache.org/) - Apache's standard data interchange format.
[Avro](https://avro.apache.org/) - Apache data serialization standart designed to work with Hadoop and the HDFS.
[JSON](https://www.json.org/) - Non binary data interchange format. 

## Testing Methods

All the plugged algorithms perform the same serialization and deserialization activity over an etherogeneus set of pre-allocated sample data. Sample data is composed by:

* A subset of objects filled by different type key-value pairs:
    - String - Long (about 20 Bytes in size)
	- String - Double (about 20 Bytes in size)
	- String - String (about 20 Bytes in size)
	- A mix of all of them (about 60 Bytes in size)
	- An empty object (about 3 Bytes in size)
- A subset of objects filled by different size strings:
	- A 50 Bytes String
	- A 250 Bytes String
	- A 1000 Bytes String
	- A 10000 Bytes String
  
All numbers are randomly generated and are forced to use all their 64 bits (i.e. the MSB is always set to 1) to avoid specific optimizations that could bias the tests.

Serialization and Deserialization operations are performed in a "JIT optimization free" way, thanks to the tools offered by the JMH suite, and are designed to avoid memory allocations that could bias the tests.

## Testing Subject

The benchmark target is to identify correlations and difference between:

* Throughput 
	* Defined by average ops/s 
	* Completed by Absolute Error (computed on the 99 percentile), Standard deviation and normal distribution
* Latency
	* Defined by the average single operation time in s/op
	* Computed on the best case, the worst case and the 50, 90, 95, 99, 99.9 and 99.99 percentile
* Size Overhead
	* Defined by unserialized / serialized payload
	* Supposing no compression
	* Computed over the set of increasing size data to identify magnitude of possible overhead 
	
## Results

A quick look on the throughput differences:

![Quick Throughtput Results](https://image.ibb.co/cOGL8o/Screenshot_from_2018_07_31_17_07_32.png)

But for the complete results you will have to download the JMH report:
https://drive.google.com/open?id=1BoyhG4awiK4RVvuNMG1vZnWNg8riNuos
You can visualize it in http://jmh.morethan.io/

## Getting Started

The project require Java, Scala and Maven to be executed. In a Debian based system, you  can install them by:
```
> sudo apt-get install open-jvm
> sudo apt-get install scala
> sudo apt-get install maven
```

Compile the project using Maven
```
mvn clean package
```

Run the project with JMH commands. You can use the command -h to see the list of all the available commands.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management


## Authors

* **Alessandro D'Armiento** - *Initial work* - 
With the help and support of all the Agile Lab team.

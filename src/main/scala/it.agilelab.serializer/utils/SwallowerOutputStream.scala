package it.agilelab.serializer.utils

import java.io.OutputStream

import org.openjdk.jmh.infra.Blackhole


class SwallowerOutputStream(blackhole: Blackhole) extends OutputStream {
  override def write(i: Int): Unit = {
    blackhole.consume(i)
  }

  override def write(bytes: Array[Byte]): Unit = {
    blackhole.consume(bytes)
  }

  override def write(bytes: Array[Byte], i: Int, i1: Int): Unit = {
    blackhole.consume(bytes)
    blackhole.consume(i)
    blackhole.consume(i1)
  }

  def write(s: String): Unit = {
    write(s.getBytes())
  }
}

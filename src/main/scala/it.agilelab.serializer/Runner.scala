package it.agilelab.serializer

import java.io.{File, FileOutputStream, FileWriter}
import java.util

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream
import com.sun.xml.internal.txw2.output.DataWriter
import it.agilelab.benchmark.avro.Payload
import it.agilelab.serializer.input.Samples.Avro._
import it.agilelab.serializer.utils.SwallowerOutputStream
import org.apache.avro.generic.GenericDatumWriter
import org.apache.avro.io.{BinaryDecoder, BinaryEncoder, DecoderFactory, EncoderFactory}


//DEBUG
object Runner extends App {

    val output = new ByteOutputStream()

    //util.Arrays.toString(output.getBytes)
    /*
        val s50 = ColferPayload.randomOfSize(50).marshal(output, new Array[Byte](51))
        println("s50: " + util.Arrays.toString(s50))
        println("s50 size: " + output.size())
        output.reset()
        val s250 = ColferPayload.randomOfSize(250).marshal(output, new Array[Byte](251))
        println("s250: " + util.Arrays.toString(s250))
        println("s250 size: " + output.size())
        output.reset()
        val s1000 = ColferPayload.randomOfSize(1000).marshal(output, new Array[Byte](1001))
        println("s1000: " + util.Arrays.toString(s1000))
        println("s1000 size: " + output.size())
        output.reset()
        val s10000 = ColferPayload.randomOfSize(9999).marshal(output, new Array[Byte](20000))
        println("s10000: " + util.Arrays.toString(s10000))
        println("s10000 size: " + output.size())

        val fout: FileOutputStream = new FileOutputStream("/tmp/colfer/s10000Sample.dat")
        fout.write(s10000);
        fout.close()

        output.reset()

        val sEmpty = ColferPayload.empty().marshal(output, new Array[Byte](5))
        println("sEmpty: " + util.Arrays.toString(sEmpty))
        println("sEmpty size: " + output.size())
        output.reset()

        val sLong = ColferPayload.randomLong().marshal(output, new Array[Byte](30))
        println("sLong: " + util.Arrays.toString(sLong))
        println("sLong size: " + output.size())
        output.reset()

        val sDouble = ColferPayload.randomDouble().marshal(output, new Array[Byte](30))
        println("sDouble: " + util.Arrays.toString(sDouble))
        println("sDouble size: " + output.size())
        output.reset()

        val sString = ColferPayload.randomString().marshal(output, new Array[Byte](30))
        println("sString: " + util.Arrays.toString(sString))
        println("sString size: " + output.size())
        output.reset()

        val sMixed = ColferPayload.randomMixed().marshal(output, new Array[Byte](70))
        println("sMixed: " + util.Arrays.toString(sMixed))
        println("sMixed size: " + output.size())
        output.reset()


          val s5000 = ColferPayload.randomOfSize(9999).marshal(output, new Array[Byte](20000))
          println("s10000: " + util.Arrays.toString(s5000))
          println("s10000 size: " + output.size())

          val fout: FileOutputStream = new FileOutputStream("/tmp/colfer/s10000Sample.dat")
          fout.write(s5000);
          fout.close()
        *

    //AvroDeserializeSuite.colferDeserialize10000B(new Blackhole("Today's password is swordfish. I understand instantiating Blackholes directly is dangerous."))
    //JsonCollapsed.protobufDeserializeDigest(new Blackhole("Today's password is swordfish. I understand instantiating Blackholes directly is dangerous."))

    val serializer = new TSerializer()

    val s50 = ThriftPayload.serialize(serializer, ThriftPayload.randomOfSize(50))
    println("s50: " + util.Arrays.toString(s50))
    println("s50 size: " + s50.length)
    output.reset()
    val s250 = ThriftPayload.serialize(serializer, ThriftPayload.randomOfSize(250))
    println("s250: " + util.Arrays.toString(s250))
    println("s250 size: " + s250.length)
    output.reset()
    val s1000 = ThriftPayload.serialize(serializer, ThriftPayload.randomOfSize(1000))
    println("s1000: " + util.Arrays.toString(s1000))
    println("s1000 size: " + s1000.length)
    output.reset()
    val s10000 = ThriftPayload.serialize(serializer, ThriftPayload.randomOfSize(10000))
    println("s10000: " + util.Arrays.toString(s10000))
    println("s10000 size: " + s10000.length)

    val fout: FileOutputStream = new FileOutputStream("/tmp/thrift/s10000Sample.dat")
    fout.write(s10000)
    fout.close()

    output.reset()

    val sEmpty = ThriftPayload.serialize(serializer, ThriftPayload.empty())
    println("sEmpty: " + util.Arrays.toString(sEmpty))
    println("sEmpty size: " + sEmpty.length)
    output.reset()

    val sLong = ThriftPayload.serialize(serializer, ThriftPayload.randomLong())
    println("sLong: " + util.Arrays.toString(sLong))
    println("sLong size: " + sLong.length)
    output.reset()

    val sDouble = ThriftPayload.serialize(serializer, ThriftPayload.randomDouble())
    println("sDouble: " + util.Arrays.toString(sDouble))
    println("sDouble size: " + sDouble.length)
    output.reset()

    val sString = ThriftPayload.serialize(serializer, ThriftPayload.randomString())
    println("sString: " + util.Arrays.toString(sString))
    println("sString size: " + sString.length)
    output.reset()

    val sMixed = ThriftPayload.serialize(serializer, ThriftPayload.randomMixed())
    println("sMixed: " + util.Arrays.toString(sMixed))
    println("sMixed size: " + sMixed.length)
    output.reset()
    *

    //val gson = new Gson
    val s50 = JSonPayload.serialize(p50Json)
    println("s50: " + s50)
    println("s50 size: " + s50.length)
    val s250 = JSonPayload.serialize(p250Json)
    println("s250: " + s250)
    println("s250 size: " + s250.length())
    val s1000 = JSonPayload.serialize(p1000Json)
    println("s1000: " + s1000)
    println("s1000 size: " + s1000.length())
    val s10000 = JSonPayload.serialize(p10000Json)
    println("s1000: " + s10000)
    println("s50 size: " + s10000.length())

    val fw: FileWriter = new FileWriter(new File("/tmp/json/s10000Sample.dat"))
    fw.write(s10000)
    fw.close()

    output.reset()

    val sEmpty = JSonPayload.serialize(pEmptyJson)
    println("sEmpty: " + sEmpty)
    println("sEmpty size: " + sEmpty.length)

    val sLong = JSonPayload.serialize(pLongJson)
    println("sLong: " + sLong)
    println("sLong size: " + sLong.length())

    val sDouble = JSonPayload.serialize(pDoubleJson)
    println("sDouble: " + sDouble)
    println("sDouble size: " + sDouble.length())

    val sString = JSonPayload.serialize(pStringJson)
    println("sString: " + sString)
    println("sString size: " + sString.length())

    val sMixed = JSonPayload.serialize(pMixedJson)
    println("sMixed: " + sMixed)
    println("sMixed size: " + sMixed.length())




    val s50 = AvroPayload.serialize(p50Avro)
    println("s50: " + util.Arrays.toString(s50))
    println("s50 size: " + s50.length)
    val s250 = AvroPayload.serialize(p250Avro)
    println("s250: " + util.Arrays.toString(s250))
    println("s250 size: " + s250.length)
    val s1000 = AvroPayload.serialize(p1000Avro)
    println("s1000: " + util.Arrays.toString(s1000))
    println("s1000 size: " + s1000.length)
    val s10000 = AvroPayload.serialize(p10000Avro)
    println("s10000: " + util.Arrays.toString(s10000))
    println("s10000 size: " + s10000.length)

    output.reset()

    val sEmpty = AvroPayload.serialize(pEmptyAvro)
    println("sEmpty: " + util.Arrays.toString(sEmpty))
    println("sEmpty size: " + sEmpty.length)

    val sLong = AvroPayload.serialize(pLongAvro)
    println("sLong: " + util.Arrays.toString(sLong))
    println("sLong size: " + sLong.length)

    val sDouble = AvroPayload.serialize(pDoubleAvro)
    println("sDouble: " + util.Arrays.toString(sDouble))
    println("sDouble size: " + sDouble.length)

    val sString = AvroPayload.serialize(pStringAvro)
    println("sString: " + util.Arrays.toString(sString))
    println("sString size: " + sString.length)

    val sMixed = AvroPayload.serialize(pMixedAvro)
    println("sMixed: " + util.Arrays.toString(sMixed))
    println("sMixed size: " + sMixed.length)
*

    val x = p10000Avro
    val y = new FileOutputStream(new File("/tmp/avro/s10000Sample.dat"))

    val encoder = EncoderFactory.get.directBinaryEncoder(y, null)
    val dataWriter = new GenericDatumWriter[Payload](Payload.getClassSchema)

    dataWriter.write(x, encoder)
*/
    //println(util.Arrays.toString(y.getBytes))

    val f = new File("resource/avro/s10000Sample.dat")
    if (f.exists())
        println("f exists")

}

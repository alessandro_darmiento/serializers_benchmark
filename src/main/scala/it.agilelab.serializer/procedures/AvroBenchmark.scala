package it.agilelab.serializer.procedures

import it.agilelab.serializer.input.{Contents, Levels}
import it.agilelab.serializer.input.payload.AvroPayload
import it.agilelab.serializer.input.Samples.Avro._
import it.agilelab.serializer.input.benchmark_suite.avro.AvroContext


object AvroBenchmark {

  def sizeDeserializeBenchmark(context: AvroContext, level: Levels.Value): Unit = {
    level match {
      case Levels.low => AvroPayload.deserialize(context, s50Avro)
      case Levels.med => AvroPayload.deserialize(context, s250Avro)
      case Levels.high => AvroPayload.deserialize(context, s1000Avro)
      case Levels.extreme => AvroPayload.deserialize(context, s10000Avro)
    }
  }

  def contentDeserializeBenchmark(context: AvroContext, content: Contents.Value): Unit = {
    content match {
      case Contents.emptyVal => AvroPayload.deserialize(context, sEmptyAvro)
      case Contents.longVal => AvroPayload.deserialize(context, sLongAvro)
      case Contents.doubleVal => AvroPayload.deserialize(context, sDoubleAvro)
      case Contents.stringVal => AvroPayload.deserialize(context, sStringAvro)
      case Contents.mixedVal => AvroPayload.deserialize(context, sMixedAvro)
    }
  }


  def sizeSerializeBenchmark(context: AvroContext, level: Levels.Value): Unit = {
    level match {
      case Levels.low => AvroPayload.serialize(context, p50Avro)
      case Levels.med => AvroPayload.serialize(context, p250Avro)
      case Levels.high => AvroPayload.serialize(context, p1000Avro)
      case Levels.extreme => AvroPayload.serialize(context, p10000Avro)
    }
  }

  def contentSerializeBenchmark(context: AvroContext, content: Contents.Value): Unit = {

    content match {
      case Contents.emptyVal => AvroPayload.serialize(context, pEmptyAvro)
      case Contents.longVal => AvroPayload.serialize(context, pLongAvro)
      case Contents.doubleVal => AvroPayload.serialize(context, pDoubleAvro)
      case Contents.stringVal => AvroPayload.serialize(context, pStringAvro)
      case Contents.mixedVal => AvroPayload.serialize(context, pMixedAvro)
    }
  }
}

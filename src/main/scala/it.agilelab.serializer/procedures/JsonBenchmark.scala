package it.agilelab.serializer.procedures

import it.agilelab.serializer.input.payload.JSonPayload
import it.agilelab.serializer.input.{Contents, Levels}
import it.agilelab.serializer.utils.SwallowerOutputStream
import org.openjdk.jmh.infra.Blackhole
import it.agilelab.serializer.input.Samples.Json._

object JsonBenchmark {

  def sizeDeserializeBenchmark(blackhole: Blackhole, level: Levels.Value): Unit = {
    val swallowerOutputStream = new SwallowerOutputStream(blackhole)

    level match {
      case Levels.low => JSonPayload.deserialize(blackhole, s50Json)
      case Levels.med => JSonPayload.deserialize(blackhole, s250Json)
      case Levels.high => JSonPayload.deserialize(blackhole, s1000Json)
      case Levels.extreme => JSonPayload.deserialize(blackhole, s10000Json)
    }
  }

  def contentDeserializeBenchmark(blackhole: Blackhole, content: Contents.Value): Unit = {
    val swallowerOutputStream = new SwallowerOutputStream(blackhole)

    content match {
      case Contents.emptyVal => JSonPayload.deserialize(blackhole, sEmptyJson)
      case Contents.longVal => JSonPayload.deserialize(blackhole, sLongJson)
      case Contents.doubleVal => JSonPayload.deserialize(blackhole, sDoubleJson)
      case Contents.stringVal => JSonPayload.deserialize(blackhole, sStringJson)
      case Contents.mixedVal => JSonPayload.deserialize(blackhole, sMixedJson)
    }
  }


  def sizeSerializeBenchmark(blackhole: Blackhole, level: Levels.Value): Unit = {
    val swallowerOutputStream = new SwallowerOutputStream(blackhole)

    level match {
      case Levels.low => JSonPayload.serialize(p50Json, swallowerOutputStream)
      case Levels.med => JSonPayload.serialize(p250Json, swallowerOutputStream)
      case Levels.high => JSonPayload.serialize(p1000Json, swallowerOutputStream)
      case Levels.extreme => JSonPayload.serialize(p10000Json, swallowerOutputStream)
    }
  }

  def contentSerializeBenchmark(blackhole: Blackhole, content: Contents.Value): Unit = {
    val swallowerOutputStream = new SwallowerOutputStream(blackhole)

    content match {
      case Contents.emptyVal => JSonPayload.serialize(pEmptyJson, swallowerOutputStream)
      case Contents.longVal => JSonPayload.serialize(pLongJson, swallowerOutputStream)
      case Contents.doubleVal => JSonPayload.serialize(pDoubleJson, swallowerOutputStream)
      case Contents.stringVal => JSonPayload.serialize(pStringJson, swallowerOutputStream)
      case Contents.mixedVal => JSonPayload.serialize(pMixedJson, swallowerOutputStream)
    }
  }

}

package it.agilelab.serializer.procedures

import it.agilelab.serializer.input.Samples.Colfer._
import it.agilelab.serializer.input.benchmark_suite.colfer.ColferContext
import it.agilelab.serializer.input.payload.ColferPayload
import it.agilelab.serializer.input.{Contents, Levels}



object ColferBenchmark {
  def sizeDeserializeBenchmark(context: ColferContext, level: Levels.Value): Unit = {
    level match {
      case Levels.low => ColferPayload.deserialize(context, s50Colfer)
      case Levels.med => ColferPayload.deserialize(context, s250Colfer)
      case Levels.high => ColferPayload.deserialize(context, s1000Colfer)
      case Levels.extreme => ColferPayload.deserialize(context, s10000Colfer)
    }
  }

  def contentDeserializeBenchmark(context: ColferContext, content: Contents.Value): Unit = {
    content match {
      case Contents.emptyVal => ColferPayload.deserialize(context, sEmptyColfer)
      case Contents.longVal => ColferPayload.deserialize(context, sLongColfer)
      case Contents.doubleVal => ColferPayload.deserialize(context, sDoubleColfer)
      case Contents.stringVal => ColferPayload.deserialize(context, sStringColfer)
      case Contents.mixedVal => ColferPayload.deserialize(context, sMixedColfer)
    }
  }


  def sizeSerializeBenchmark(context: ColferContext, level: Levels.Value): Unit = {
    level match {
      case Levels.low => ColferPayload.serialize(context, p50Colfer)
      case Levels.med => ColferPayload.serialize(context, p250Colfer)
      case Levels.high => ColferPayload.serialize(context, p1000Colfer)
      case Levels.extreme => ColferPayload.serialize(context, p10000Colfer)
    }
  }

  def contentSerializeBenchmark(context: ColferContext, content: Contents.Value): Unit = {
    content match {
      case Contents.emptyVal => ColferPayload.serialize(context, pEmptyColfer)
      case Contents.longVal => ColferPayload.serialize(context, pLongColfer)
      case Contents.doubleVal => ColferPayload.serialize(context, pDoubleColfer)
      case Contents.stringVal => ColferPayload.serialize(context, pStringColfer)
      case Contents.mixedVal => ColferPayload.serialize(context, pMixedColfer)
    }
  }
}

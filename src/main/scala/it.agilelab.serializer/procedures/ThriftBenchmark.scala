package it.agilelab.serializer.procedures

import it.agilelab.serializer.input.payload.ThriftPayload
import it.agilelab.serializer.input.{Contents, Levels}
import it.agilelab.serializer.input.Samples.Thrift._
import it.agilelab.serializer.input.benchmark_suite.thrift.ThriftContext

object ThriftBenchmark {


  def sizeDeserializeBenchmark(context: ThriftContext, level: Levels.Value): Unit = {

    level match {
      case Levels.low => ThriftPayload.deserialize(context, s50Thrift)
      case Levels.med => ThriftPayload.deserialize(context, s250Thrift)
      case Levels.high => ThriftPayload.deserialize(context, s1000Thrift)
      case Levels.extreme => ThriftPayload.deserialize(context, s10000Thrift)
    }
  }

  def contentDeserializeBenchmark(context: ThriftContext, content: Contents.Value): Unit = {

    content match {
      case Contents.emptyVal => ThriftPayload.deserialize(context, sEmptyThrift)
      case Contents.longVal => ThriftPayload.deserialize(context, sLongThrift)
      case Contents.doubleVal => ThriftPayload.deserialize(context, sDoubleThrift)
      case Contents.stringVal => ThriftPayload.deserialize(context, sStringThrift)
      case Contents.mixedVal => ThriftPayload.deserialize(context, sMixedThrift)
    }
  }


  def sizeSerializeBenchmark(context: ThriftContext, level: Levels.Value): Unit = {
    level match {
      case Levels.low => ThriftPayload.serialize(context, p50Thrift)
      case Levels.med => ThriftPayload.serialize(context, p250Thrift)
      case Levels.high => ThriftPayload.serialize(context, p1000Thrift)
      case Levels.extreme => ThriftPayload.serialize(context, p10000Thrift)
    }
  }

  def contentSerializeBenchmark(context: ThriftContext, content: Contents.Value): Unit = {
    content match {
      case Contents.emptyVal => ThriftPayload.serialize(context, pEmptyThrift)
      case Contents.longVal => ThriftPayload.serialize(context, pLongThrift)
      case Contents.doubleVal => ThriftPayload.serialize(context, pDoubleThrift)
      case Contents.stringVal => ThriftPayload.serialize(context, pStringThrift)
      case Contents.mixedVal => ThriftPayload.serialize(context, pMixedThrift)
    }
  }
}

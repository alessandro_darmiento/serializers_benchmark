package it.agilelab.serializer.procedures

import it.agilelab.serializer.input.Samples.Protobuf._
import it.agilelab.serializer.input.payload.ProtobufPayload
import it.agilelab.serializer.input.{Contents, Levels, Samples}
import it.agilelab.serializer.utils.SwallowerOutputStream
import org.openjdk.jmh.infra.Blackhole

object ProtoBenchmark {


  def sizeDeserializeBenchmark(blackhole: Blackhole, level: Levels.Value): Unit = {
    level match {
      case Levels.low => ProtobufPayload.deserialize(blackhole, s50Protobuf)
      case Levels.med => ProtobufPayload.deserialize(blackhole, s250Protobuf)
      case Levels.high => ProtobufPayload.deserialize(blackhole, s1000Protobuf)
      case Levels.extreme => ProtobufPayload.deserialize(blackhole, s10000Protobuf)
    }
  }

  def contentDeserializeBenchmark(blackhole: Blackhole, content: Contents.Value): Unit = {
    content match {
      case Contents.emptyVal => ProtobufPayload.deserialize(blackhole, sEmptyProtobuf)
      case Contents.longVal => ProtobufPayload.deserialize(blackhole, sLongProtobuf)
      case Contents.doubleVal => ProtobufPayload.deserialize(blackhole, sDoubleProtobuf)
      case Contents.stringVal => ProtobufPayload.deserialize(blackhole, sStringProtobuf)
      case Contents.mixedVal => ProtobufPayload.deserialize(blackhole, sMixedProtobuf)
    }
  }




  def sizeSerializeBenchmark(blackhole: Blackhole, level: Levels.Value): Unit = {
    val swallowerOutputStream = new SwallowerOutputStream(blackhole)

    level match {
      case Levels.low => ProtobufPayload.serialize(swallowerOutputStream, p50Protobuf)
      case Levels.med => ProtobufPayload.serialize(swallowerOutputStream, p250Protobuf)
      case Levels.high => ProtobufPayload.serialize(swallowerOutputStream, p1000Protobuf)
      case Levels.extreme => ProtobufPayload.serialize(swallowerOutputStream, p10000Protobuf)
    }
  }

  def contentSerializeBenchmark(blackhole: Blackhole, content: Contents.Value): Unit = {
    val swallowerOutputStream = new SwallowerOutputStream(blackhole)

    content match {
      case Contents.emptyVal => ProtobufPayload.serialize(swallowerOutputStream, pEmptyProtobuf)
      case Contents.longVal => ProtobufPayload.serialize(swallowerOutputStream, pLongProtobuf)
      case Contents.doubleVal => ProtobufPayload.serialize(swallowerOutputStream, pDoubleProtobuf)
      case Contents.stringVal => ProtobufPayload.serialize(swallowerOutputStream, pStringProtobuf)
      case Contents.mixedVal => ProtobufPayload.serialize(swallowerOutputStream, pMixedProtobuf)
    }
  }
}

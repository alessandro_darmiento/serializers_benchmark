package it.agilelab.serializer.input.payload


import it.agilelab.benchmark.colfer.{Payload => ColfP}
import it.agilelab.serializer.input.RandomUtil
import it.agilelab.serializer.input.benchmark_suite.colfer.ColferContext

object ColferPayload {

  final val rand = new RandomUtil()

  def randomMixed(): ColfP = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val longVal = rand.nextBigLong() //A random value of at least 63 bits
    var p = new ColfP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("MyString")
    p.setStringValue(rand.nextString())
    p.setFloatKey("MyFLoat")
    p.setFloatValue(doubleVal)
    p.setIntKey("MyInt")
    p.setIntValue(longVal)
    p
  }

  def randomDouble(): ColfP = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val p = new ColfP()
    p.setId(rand.nextInt(5).toString)
    p.setFloatKey("MyFLoat")
    p.setFloatValue(doubleVal)
    p
  }

  def randomLong(): ColfP = {
    val longVal = rand.nextBigLong() //A random value of at least 63 bits
    val p = new ColfP()
    p.setId(rand.nextInt(5).toString)
    p.setIntKey("MyInt")
    p.setIntValue(longVal)
    p
  }

  def randomString(): ColfP = {
    var p = new ColfP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("MyString")
    p.setStringValue(rand.nextString())
    p
  }

  def empty(): ColfP = {
    val p = new ColfP()
    p.setId(rand.nextInt(5).toString)
    p
  }

  def randomOfSize(size: Int): ColfP = {
    //Approx 30 bytes for header
    val nOfChars: Int = Math.max(1, size - 30)
    val p = new ColfP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("StringOfSize " + size + " byte")
    p.setStringValue(rand.nextString(nOfChars))
    p
  }

  def deserialize(context: ColferContext, bytes: Array[Byte]): Unit = {
    context.getBlackhole.consume(context.getRecipient.unmarshal(bytes, 0))
  }

  def serialize(context: ColferContext, payload: ColfP): Unit = {
    payload.marshal(context.getOutputStream, context.getBuffer)
  }

}

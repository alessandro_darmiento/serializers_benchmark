package it.agilelab.serializer.input.payload

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, OutputStream}

import it.agilelab.benchmark.avro.{Payload => AvroP}
import it.agilelab.serializer.input.RandomUtil
import it.agilelab.serializer.input.benchmark_suite.avro.AvroContext
import it.agilelab.serializer.utils.SwallowerOutputStream
import org.apache.avro.generic.{GenericDatumReader, GenericDatumWriter, GenericRecord}
import org.apache.avro.io._
import org.openjdk.jmh.infra.Blackhole

object AvroPayload {

  final val rand = new RandomUtil()

  final val dataWriter = new GenericDatumWriter[AvroP](AvroP.getClassSchema)
  final val dataReader = new GenericDatumReader[GenericRecord](AvroP.getClassSchema)

  //WARN: Builder can be used only if you set ALL values
  def randomMixed(): AvroP = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val longVal = rand.nextBigLong() //A random value of at least 63 bits

    AvroP.newBuilder().setId(rand.nextInt(5).toString).setStringKey("MyString").setStringValue(rand.nextString())
      .setFloatKey("MyFLoat").setFloatValue(doubleVal).setIntKey("MyInt").setIntValue(longVal).build()
  }

  def randomDouble(): AvroP = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    //Example with builder
    AvroP.newBuilder().setId(rand.nextInt(5).toString).setStringKey(null).setStringValue(null)
      .setFloatKey("MyFLoat").setFloatValue(doubleVal).setIntKey(null).setIntValue(null).build()
  }

  def randomLong(): AvroP = {
    val longVal = rand.nextBigLong() //A random value of at least 63 bits
    //Example with builder
    AvroP.newBuilder().setId(rand.nextInt(5).toString).setStringKey(null).setStringValue(null)
      .setFloatKey(null).setFloatValue(null).setIntKey("MyInt").setIntValue(longVal).build()
  }

  def randomString(): AvroP = {
    //Example without builder
    val p = new AvroP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("MyString")
    p.setStringValue(rand.nextString())
    p
  }

  def empty(): AvroP = {
    //Example without builder
    val p = new AvroP()
    p.setId(rand.nextInt(5).toString)
    p
  }

  def randomOfSize(size: Int): AvroP = {
    val nOfChars: Int = Math.max(1, size - 30)

    val p = new AvroP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("StringOfSize " + size + " byte")
    p.setStringValue(rand.nextString(nOfChars))
    p
  }

  def serialize(context: AvroContext, payload: AvroP): Unit = {
    dataWriter.write(payload, context.getEncoder)
    //encoder.flush()
  }


  def deserialize(context: AvroContext, bytes: Array[Byte]): Unit = {
    val myDecoder = DecoderFactory.get().binaryDecoder(bytes, context.getDecoder)
    context.getBlackhole.consume(dataReader.read(null, myDecoder))
  }

}

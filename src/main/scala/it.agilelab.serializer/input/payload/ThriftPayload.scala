package it.agilelab.serializer.input.payload

import it.agilelab.benchmark.thrift.{Payload => ThriftP}
import it.agilelab.serializer.input.RandomUtil
import it.agilelab.serializer.input.benchmark_suite.thrift.ThriftContext
import org.apache.thrift.protocol.TProtocol
import org.apache.thrift.{TDeserializer, TSerializer}
import org.openjdk.jmh.infra.Blackhole

object ThriftPayload {

  final val rand = new RandomUtil()

  def randomMixed(): ThriftP = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val longVal = rand.nextBigLong() //A random value of at least 63 bits
    var p = new ThriftP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("MyString")
    p.setStringValue(rand.nextString())
    p.setFloatKey("MyFLoat")
    p.setFloatValue(doubleVal)
    p.setIntKey("MyInt")
    p.setIntValue(longVal)
    p
  }

  def randomDouble(): ThriftP = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val p = new ThriftP()
    p.setId(rand.nextInt(5).toString)
    p.setFloatKey("MyFLoat")
    p.setFloatValue(doubleVal)
    p
  }

  def randomLong(): ThriftP = {
    val longVal = rand.nextBigLong() //A random value of at least 63 bits
    val p = new ThriftP()
    p.setId(rand.nextInt(5).toString)
    p.setIntKey("MyInt")
    p.setIntValue(longVal)
    p
  }

  def randomString(): ThriftP = {
    var p = new ThriftP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("MyString")
    p.setStringValue(rand.nextString())
    p
  }

  def empty(): ThriftP = {
    val p = new ThriftP()
    p.setId(rand.nextInt(5).toString)
    p
  }

  def randomOfSize(size: Int): ThriftP = {
    //Approx 30 bytes for header
    val nOfChars: Int = Math.max(1, size - 30)
    val p = new ThriftP()
    p.setId(rand.nextInt(5).toString)
    p.setStringKey("StringOfSize " + size + " byte")
    p.setStringValue(rand.nextString(nOfChars))
    p
  }

  def deserialize(context: ThriftContext, bytes: Array[Byte]): Unit = {
    context.getBlackhole.consume(context.getDeserializer.deserialize(context.getRecipient, bytes))
  }

  def serialize(context: ThriftContext, payload: ThriftP): Unit = {
    payload.write(context.getProtocol)
    context.getProtocol.getTransport.flush()
  }


}

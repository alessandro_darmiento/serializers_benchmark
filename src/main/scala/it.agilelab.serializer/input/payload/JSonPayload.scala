package it.agilelab.serializer.input.payload

import java.io.OutputStream

import it.agilelab.serializer.input.RandomUtil
import org.json4s._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.write
import org.json4s.jackson.Serialization.read
import org.openjdk.jmh.infra.Blackhole

//import scala.sys.process.processInternal.OutputStream


case class JSonPayload(id: String, stringParam: Option[(String, String)], longParam: Option[(String, Long)], doubleParam: Option[(String, Double)]) {
  override def toString: String = {
    s"$id: $stringParam, $longParam, $doubleParam"
  }

}

object JSonPayload {
  final val rand = new RandomUtil()
  implicit val formats = DefaultFormats

  def randomMixed(): JSonPayload = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val longVal = rand.nextBigLong() //A random value of at least 63 bits

    new JSonPayload(rand.nextInt(5).toString, Some("MyString", rand.nextString()), Some("MyInt", longVal), Some("MyFLoat", doubleVal))
  }

  def randomDouble(): JSonPayload = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits

    new JSonPayload(rand.nextInt(5).toString, None, None, Some("MyFLoat", doubleVal))
  }

  def randomLong(): JSonPayload = {
    val longVal = rand.nextBigLong() //A random value of at least 63 bits

    new JSonPayload(rand.nextInt(5).toString, None, Some("MyInt", longVal), None)
  }

  def randomString(): JSonPayload = {
    new JSonPayload(rand.nextInt(5).toString, Some("MyString", rand.nextString()), None, None)
  }

  def empty(): JSonPayload = {
    new JSonPayload(rand.nextInt(5).toString, None, None, None)
  }

  def randomOfSize(size: Int): JSonPayload = {
    //Approx 30 bytes for header
    val nOfChars: Int = Math.max(1, size - 30)
    new JSonPayload(rand.nextInt(5).toString, Some("MyString", rand.nextString(nOfChars)), None, None)
  }

  def serialize(payload: JSonPayload, out: OutputStream): Unit = {
    write(payload, out)
  }

  def deserialize(blackhole: Blackhole, jString: String): Unit = {
    blackhole.consume(read[JSonPayload](jString))
  }

}

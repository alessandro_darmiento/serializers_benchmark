package it.agilelab.serializer.input.payload

import java.io.{FileInputStream, OutputStream}

import it.agilelab.benchmark.protobuf.PayloadOuterClass.{Payload => ProtoPayload}
import it.agilelab.serializer.input.RandomUtil
import org.openjdk.jmh.infra.Blackhole


object ProtobufPayload {

  final val rand = new RandomUtil()


  def randomMixed(): ProtoPayload = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits
    val longVal = rand.nextBigLong() //A random value of at least 63 bits

    ProtoPayload.newBuilder().setId(rand.nextInt(5).toString).setStringKey("MyString").setStringValue(rand.nextString())
      .setFloatKey("MyFLoat").setFloatValue(doubleVal).setIntKey("MyInt").setIntValue(longVal).build()
  }

  def randomDouble(): ProtoPayload = {
    val doubleVal = rand.nextBigDouble() //A random value of at least 63 bits

    ProtoPayload.newBuilder().setId(rand.nextInt(5).toString).setFloatKey("MyFLoat").setFloatValue(doubleVal).build()
  }

  def randomLong(): ProtoPayload = {
    val longVal = rand.nextBigLong() //A random value of at least 63 bits

    ProtoPayload.newBuilder().setId(rand.nextInt(5).toString).setIntKey("MyInt").setIntValue(longVal).build()
  }

  def randomString(): ProtoPayload = {
    ProtoPayload.newBuilder().setId(rand.nextInt(5).toString).setStringKey("MyString").setStringValue(rand.nextString()).build()
  }

  def empty(): ProtoPayload = {
    ProtoPayload.newBuilder().setId(rand.nextInt(5).toString).build()
  }

  def randomOfSize(size: Int): ProtoPayload = {
    //Approx 30 bytes for header
    val nOfChars: Int = Math.max(1, size - 30)
    ProtoPayload.newBuilder().setId(rand.nextInt(5).toString).setStringKey("StringOfSize " + size + " byte")
      .setStringValue(rand.nextString(nOfChars)).build()
  }


  def deserialize(blackhole: Blackhole, bytes: Array[Byte]): Unit = {
    blackhole.consume(ProtoPayload.parseFrom(bytes))
  }

  def serialize(outputStream: OutputStream, payload: ProtoPayload): Unit = {
    payload.writeTo(outputStream)
  }

}

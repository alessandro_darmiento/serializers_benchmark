namespace java it.agilelab.benchmark.thrift

struct Payload {
  1: required string id
  2: optional string stringKey
  3: optional string stringValue
  4: optional string intKey
  5: optional i64 intValue
  6: optional string floatKey
  7: optional double floatValue
}
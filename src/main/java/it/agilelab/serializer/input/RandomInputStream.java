package it.agilelab.serializer.input;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class RandomInputStream extends InputStream {
    private int available;
    private Random random = new Random();

    public RandomInputStream(final int totalSize) {
        this.available = totalSize;
    }

    @Override
    public int read() throws IOException {
        if (available == 0)
            return -1;
        --available;
        return random.nextInt(256);
    }


    @Override
    public int available() throws IOException {
        return available;
    }
}
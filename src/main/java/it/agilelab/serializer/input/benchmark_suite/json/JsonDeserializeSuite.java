package it.agilelab.serializer.input.benchmark_suite.json;


import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.JsonBenchmark;
import it.agilelab.serializer.procedures.ProtoBenchmark;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

public class JsonDeserializeSuite {


    @Benchmark
    public static void jsonDeserialize50B(Blackhole blackhole) {
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.low());
    }


    @Benchmark
    public static void jsonDeserialize250B(Blackhole blackhole) {
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.med());
    }


    @Benchmark
    public static void jsonDeserialize1000B(Blackhole blackhole) {
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.high());
    }


    @Benchmark
    public static void jsonDeserialize10000B(Blackhole blackhole) {
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.extreme());
    }


    @Benchmark
    public static void jsonDeserializeEmpty(Blackhole blackhole) {
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.emptyVal());
    }


    @Benchmark
    public static void jsonDeserializeLong(Blackhole blackhole) {
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.longVal());
    }


    @Benchmark
    public static void jsonDeserializeString(Blackhole blackhole) {
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.stringVal());
    }


    @Benchmark
    public static void jsonDeserializeDouble(Blackhole blackhole) {
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.doubleVal());
    }


    @Benchmark
    public static void jsonDeserializeMixed(Blackhole blackhole) {
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.mixedVal());
    }
}




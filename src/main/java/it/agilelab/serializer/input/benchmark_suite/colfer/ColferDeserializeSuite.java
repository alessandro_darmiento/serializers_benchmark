package it.agilelab.serializer.input.benchmark_suite.colfer;

import it.agilelab.serializer.procedures.ColferBenchmark;
import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

public class ColferDeserializeSuite {


    @Benchmark
    public static void colferDeserialize50B(ColferContext context) {
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.low());
    }


    @Benchmark
    public static void colferDeserialize250B(ColferContext context) {
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.med());
    }


    @Benchmark
    public static void colferDeserialize1000B(ColferContext context) {
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.high());
    }


    @Benchmark
    public static void colferDeserialize10000B(ColferContext context) {
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.extreme());
    }


    @Benchmark
    public static void colferDeserializeEmpty(ColferContext context) {
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.emptyVal());
    }


    @Benchmark
    public static void colferDeserializeLong(ColferContext context) {
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.longVal());
    }


    @Benchmark
    public static void colferDeserializeString(ColferContext context) {
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.stringVal());
    }


    @Benchmark
    public static void colferDeserializeDouble(ColferContext context) {
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.doubleVal());
    }


    @Benchmark
    public static void colferDeserializeMixed(ColferContext context) {
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }

}

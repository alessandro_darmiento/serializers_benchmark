package it.agilelab.serializer.input.benchmark_suite.thrift;

import it.agilelab.benchmark.thrift.Payload;
import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.ThriftBenchmark;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TSerializer;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

public class ThriftCollapsed {

    @Benchmark
    public static void thriftSerializeDigest(ThriftContext context) {
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.low());
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.med());
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.high());
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.extreme());

        ThriftBenchmark.contentSerializeBenchmark(context, Contents.emptyVal());
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.longVal());
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.doubleVal());
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.stringVal());
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }

    @Benchmark
    public static void thriftDeserializeDigest(ThriftContext context) {
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.low());
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.med());
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.high());
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.extreme());

        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.emptyVal());
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.longVal());
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.doubleVal());
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.stringVal());
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }
}

package it.agilelab.serializer.input.benchmark_suite.protobuf;

import it.agilelab.serializer.procedures.ProtoBenchmark;
import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

/**
 * Execute tests with low granularity level
 */
public class ProtobufCollapsed {

    @Benchmark
    public static void protobufSerializeDigest(Blackhole blackhole) {
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.low());
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.med());
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.high());
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.extreme());

        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.emptyVal());
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.longVal());
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.doubleVal());
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.stringVal());
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.mixedVal());
    }

    @Benchmark
    public static void protobufDeserializeDigest(Blackhole blackhole) {
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.low());
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.med());
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.high());
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.extreme());

        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.emptyVal());
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.longVal());
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.doubleVal());
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.stringVal());
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.mixedVal());
    }

}

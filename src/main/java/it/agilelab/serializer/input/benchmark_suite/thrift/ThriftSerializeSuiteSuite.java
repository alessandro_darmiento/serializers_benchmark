package it.agilelab.serializer.input.benchmark_suite.thrift;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.ThriftBenchmark;
import org.apache.thrift.TSerializer;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

public class ThriftSerializeSuiteSuite {

    @Benchmark
    public static void thriftSerialize50B(ThriftContext context) {
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.low());
    }


    @Benchmark
    public static void thriftSerialize250B(ThriftContext context) {
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.med());
    }

    @Benchmark
    public static void thriftSerialize1000B(ThriftContext context) {
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.high());
    }

    @Benchmark
    public static void thriftSerialize10000B(ThriftContext context) {
        ThriftBenchmark.sizeSerializeBenchmark(context, Levels.extreme());
    }

    @Benchmark
    public static void thriftSerializeEmpty(ThriftContext context) {
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.emptyVal());
    }

    @Benchmark
    public static void thriftSerializeLong(ThriftContext context) {
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.longVal());
    }

    @Benchmark
    public static void thriftSerializeDouble(ThriftContext context) {
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.doubleVal());
    }

    @Benchmark
    public static void thriftSerializeString(ThriftContext context) {
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.stringVal());
    }

    @Benchmark
    public static void thriftSerializeMixed(ThriftContext context) {
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }


}

package it.agilelab.serializer.input.benchmark_suite.protobuf;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.ProtoBenchmark;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

public class ProtobufSerializeSuite {

    @Benchmark
    public static void protobufSerialize50B(Blackhole blackhole) {
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.low());
    }


    @Benchmark
    public static void protobufSerialize250B(Blackhole blackhole) {
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.med());
    }


    @Benchmark
    public static void protobufSerialize1000B(Blackhole blackhole) {
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.high());
    }


    @Benchmark
    public static void protobufSerialize10000B(Blackhole blackhole) {
        ProtoBenchmark.sizeSerializeBenchmark(blackhole, Levels.extreme());
    }


    @Benchmark
    public static void protobufSerializeEmpty(Blackhole blackhole) {
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.emptyVal());
    }


    @Benchmark
    public static void protobufSerializeLong(Blackhole blackhole) {
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.longVal());
    }


    @Benchmark
    public static void protobufSerializeString(Blackhole blackhole) {
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.stringVal());
    }


    @Benchmark
    public static void protobufSerializeDouble(Blackhole blackhole) {
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.doubleVal());
    }


    @Benchmark
    public static void protobufSerializeMixed(Blackhole blackhole) {
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.mixedVal());
    }


}

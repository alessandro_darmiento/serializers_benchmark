package it.agilelab.serializer.input.benchmark_suite.thrift;


import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.ThriftBenchmark;
import org.openjdk.jmh.annotations.Benchmark;


public class ThriftDeserializeSuite {


    @Benchmark
    public static void thriftDeserialize50B(ThriftContext context) {
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.low());
    }


    @Benchmark
    public static void thriftDeserialize250B(ThriftContext context) {
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.med());
    }


    @Benchmark
    public static void thriftDeserialize1000B(ThriftContext context) {
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.high());
    }


    @Benchmark
    public static void thriftDeserialize10000B(ThriftContext context) {
        ThriftBenchmark.sizeDeserializeBenchmark(context, Levels.extreme());
    }


    @Benchmark
    public static void thriftDeserializeEmpty(ThriftContext context) {
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.emptyVal());
    }


    @Benchmark
    public static void thriftDeserializeLong(ThriftContext context) {
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.longVal());
    }


    @Benchmark
    public static void thriftDeserializeString(ThriftContext context) {
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.stringVal());
    }


    @Benchmark
    public static void thriftDeserializeDouble(ThriftContext context) {
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.doubleVal());
    }


    @Benchmark
    public static void thriftDeserializeMixed(ThriftContext context) {
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }
}

package it.agilelab.serializer.input.benchmark_suite.thrift;

import it.agilelab.benchmark.thrift.Payload;
import it.agilelab.serializer.utils.SwallowerOutputStream;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.io.OutputStream;

@State(Scope.Thread)
public class ThriftContext {

    private TProtocol protocol;
    private Payload recipient;
    private Blackhole blackhole;
    private TDeserializer deserializer;
    private TSerializer serializer;

    @Setup
    public void init(Blackhole bl) {
        blackhole = bl;
        OutputStream out = new SwallowerOutputStream(blackhole);
        TCompactProtocol.Factory protocolFactory = new TCompactProtocol.Factory();
        protocol = protocolFactory.getProtocol(new TIOStreamTransport(out));
        recipient = new Payload();

        serializer = new TSerializer();
        deserializer = new TDeserializer();
    }


    public TProtocol getProtocol() {
        return protocol;
    }

    public Payload getRecipient() {
        return recipient;
    }

    public Blackhole getBlackhole() {
        return blackhole;
    }

    public TDeserializer getDeserializer() {
        return deserializer;
    }

    public TSerializer getSerializer() {
        return serializer;
    }
}

package it.agilelab.serializer.input.benchmark_suite.json;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.JsonBenchmark;
import it.agilelab.serializer.procedures.ProtoBenchmark;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

public class JsonSerializeSuite {

    @Benchmark
    public static void jsonSerialize50B(Blackhole blackhole) {
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.low());
    }


    @Benchmark
    public static void jsonSerialize250B(Blackhole blackhole) {
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.med());
    }


    @Benchmark
    public static void jsonSerialize1000B(Blackhole blackhole) {
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.high());
    }


    @Benchmark
    public static void jsonSerialize10000B(Blackhole blackhole) {
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.extreme());
    }


    @Benchmark
    public static void jsonSerializeEmpty(Blackhole blackhole) {
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.emptyVal());
    }


    @Benchmark
    public static void jsonSerializeLong(Blackhole blackhole) {
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.longVal());
    }


    @Benchmark
    public static void jsonSerializeString(Blackhole blackhole) {
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.stringVal());
    }


    @Benchmark
    public static void jsonSerializeDouble(Blackhole blackhole) {
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.doubleVal());
    }


    @Benchmark
    public static void jsonSerializeMixed(Blackhole blackhole) {
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.mixedVal());
    }


}

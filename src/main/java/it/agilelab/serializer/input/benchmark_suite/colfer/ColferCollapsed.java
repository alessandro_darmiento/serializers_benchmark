package it.agilelab.serializer.input.benchmark_suite.colfer;

import it.agilelab.serializer.procedures.ColferBenchmark;
import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

public class ColferCollapsed {

    @Benchmark
    public static void colferSerializeDigest(ColferContext context) {
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.low());
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.med());
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.high());
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.extreme());

        ColferBenchmark.contentSerializeBenchmark(context, Contents.emptyVal());
        ColferBenchmark.contentSerializeBenchmark(context, Contents.longVal());
        ColferBenchmark.contentSerializeBenchmark(context, Contents.doubleVal());
        ColferBenchmark.contentSerializeBenchmark(context, Contents.stringVal());
        ColferBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }

    @Benchmark
    public static void colferDeserializeDigest(ColferContext context) {
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.low());
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.med());
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.high());
        ColferBenchmark.sizeDeserializeBenchmark(context, Levels.extreme());

        ColferBenchmark.contentDeserializeBenchmark(context, Contents.emptyVal());
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.longVal());
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.doubleVal());
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.stringVal());
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }
}

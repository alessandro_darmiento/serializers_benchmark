package it.agilelab.serializer.input.benchmark_suite.protobuf;


import it.agilelab.serializer.procedures.ProtoBenchmark;
import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

public class ProtobufDeserializeSuite {


    @Benchmark
    public static void protobufDeserialize50B(Blackhole blackhole) {
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.low());
    }


    @Benchmark
    public static void protobufDeserialize250B(Blackhole blackhole) {
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.med());
    }


    @Benchmark
    public static void protobufDeserialize1000B(Blackhole blackhole) {
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.high());
    }


    @Benchmark
    public static void protobufDeserialize10000B(Blackhole blackhole) {
        ProtoBenchmark.sizeDeserializeBenchmark(blackhole, Levels.extreme());
    }


    @Benchmark
    public static void protobufDeserializeEmpty(Blackhole blackhole) {
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.emptyVal());
    }


    @Benchmark
    public static void protobufDeserializeLong(Blackhole blackhole) {
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.longVal());
    }


    @Benchmark
    public static void protobufDeserializeString(Blackhole blackhole) {
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.stringVal());
    }


    @Benchmark
    public static void protobufDeserializeDouble(Blackhole blackhole) {
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.doubleVal());
    }


    @Benchmark
    public static void protobufDeserializeMixed(Blackhole blackhole) {
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.mixedVal());
    }
}




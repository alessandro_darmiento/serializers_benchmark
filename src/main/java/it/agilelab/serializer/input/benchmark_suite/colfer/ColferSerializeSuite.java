package it.agilelab.serializer.input.benchmark_suite.colfer;

import it.agilelab.serializer.procedures.ColferBenchmark;
import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

public class ColferSerializeSuite {


    @Benchmark
    public static void colferSerialize50B(ColferContext context) {
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.low());
    }


    @Benchmark
    public static void colferSerialize250B(ColferContext context) {
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.med());
    }

    @Benchmark
    public static void colferSerialize1000B(ColferContext context) {
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.high());
    }

    @Benchmark
    public static void colferSerialize10000B(ColferContext context) {
        ColferBenchmark.sizeSerializeBenchmark(context, Levels.extreme());
    }

    @Benchmark
    public static void colferSerializeEmpty(ColferContext context) {
        ColferBenchmark.contentSerializeBenchmark(context, Contents.emptyVal());
    }

    @Benchmark
    public static void colferSerializeLong(ColferContext context) {
        ColferBenchmark.contentSerializeBenchmark(context, Contents.longVal());
    }

    @Benchmark
    public static void colferSerializeDouble(ColferContext context) {
        ColferBenchmark.contentSerializeBenchmark(context, Contents.doubleVal());
    }

    @Benchmark
    public static void colferSerializeString(ColferContext context) {
        ColferBenchmark.contentSerializeBenchmark(context, Contents.stringVal());
    }

    @Benchmark
    public static void colferSerializeMixed(ColferContext context) {
        ColferBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }

}

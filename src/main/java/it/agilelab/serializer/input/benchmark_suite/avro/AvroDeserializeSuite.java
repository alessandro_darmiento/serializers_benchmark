package it.agilelab.serializer.input.benchmark_suite.avro;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.AvroBenchmark;
import it.agilelab.serializer.procedures.AvroBenchmark;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

public class AvroDeserializeSuite {


    @Benchmark
    public static void avroDeserialize50B(AvroContext context) {
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.low());
    }


    @Benchmark
    public static void avroDeserialize250B(AvroContext context) {
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.med());
    }


    @Benchmark
    public static void avroDeserialize1000B(AvroContext context) {
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.high());
    }


    @Benchmark
    public static void avroDeserialize10000B(AvroContext context) {
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.extreme());
    }


    @Benchmark
    public static void avroDeserializeEmpty(AvroContext context) {
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.emptyVal());
    }


    @Benchmark
    public static void avroDeserializeLong(AvroContext context) {
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.longVal());
    }


    @Benchmark
    public static void avroDeserializeString(AvroContext context) {
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.stringVal());
    }


    @Benchmark
    public static void avroDeserializeDouble(AvroContext context) {
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.doubleVal());
    }


    @Benchmark
    public static void avroDeserializeMixed(AvroContext context) {
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }

}

package it.agilelab.serializer.input.benchmark_suite;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.benchmark_suite.avro.AvroContext;
import it.agilelab.serializer.input.benchmark_suite.colfer.ColferContext;
import it.agilelab.serializer.input.benchmark_suite.thrift.ThriftContext;
import it.agilelab.serializer.procedures.*;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

/**
 * Sigle benchmark suite which serialize and deserialize a random_mixed payload with different standards
 */
public class FastComparison {

    @Benchmark
    public static void avroDeserialize(AvroContext context) {
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());

    }

    @Benchmark
    public static void avroSerialize(AvroContext context) {
        AvroBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());

    }


    @Benchmark
    public static void colferDeserializeMixed(ColferContext context) {
        ColferBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }

    @Benchmark
    public static void colferSerializeMixed(ColferContext context) {
        ColferBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }

    @Benchmark
    public static void jsonDeserializeMixed(Blackhole blackhole) {
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.mixedVal());
    }

    @Benchmark
    public static void jsonSerializeMixed(Blackhole blackhole) {
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.mixedVal());
    }


    @Benchmark
    public static void protobufDeserializeMixed(Blackhole blackhole) {
        ProtoBenchmark.contentDeserializeBenchmark(blackhole, Contents.mixedVal());
    }

    @Benchmark
    public static void protobufSerializeMixed(Blackhole blackhole) {
        ProtoBenchmark.contentSerializeBenchmark(blackhole, Contents.mixedVal());
    }

    @Benchmark
    public static void thriftDeserializeMixed(ThriftContext context) {
        ThriftBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }

    @Benchmark
    public static void thriftSerializeMixed(ThriftContext context) {
        ThriftBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }
}

package it.agilelab.serializer.input.benchmark_suite.colfer;

import it.agilelab.benchmark.colfer.Payload;
import it.agilelab.serializer.utils.SwallowerOutputStream;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.io.OutputStream;

@State(Scope.Thread)
public class ColferContext {

    private Payload recipient;
    private byte[] buffer;
    private OutputStream outputStream;
    private Blackhole blackhole;

    @Setup
    public void init(Blackhole bl) {
        blackhole = bl;
        recipient = new Payload();
        buffer = new byte[1000000];
        outputStream = new SwallowerOutputStream(blackhole);
    }

    public Payload getRecipient() {
        return recipient;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public Blackhole getBlackhole() {
        return blackhole;
    }
}

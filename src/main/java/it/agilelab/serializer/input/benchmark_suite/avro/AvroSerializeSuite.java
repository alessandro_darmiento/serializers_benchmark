package it.agilelab.serializer.input.benchmark_suite.avro;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.AvroBenchmark;
import it.agilelab.serializer.utils.SwallowerOutputStream;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.EncoderFactory;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.io.OutputStream;

public class AvroSerializeSuite {

    @Benchmark
    public static void avroSerialize50B(AvroContext context) {
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.low());
    }


    @Benchmark
    public static void avroSerialize250B(AvroContext context) {
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.med());
    }

    @Benchmark
    public static void avroSerialize1000B(AvroContext context) {
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.high());
    }

    @Benchmark
    public static void avroSerialize10000B(AvroContext context) {
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.extreme());
    }

    @Benchmark
    public static void avroSerializeEmpty(AvroContext context) {
        AvroBenchmark.contentSerializeBenchmark(context, Contents.emptyVal());
    }

    @Benchmark
    public static void avroSerializeLong(AvroContext context) {
        AvroBenchmark.contentSerializeBenchmark(context, Contents.longVal());
    }

    @Benchmark
    public static void avroSerializeDouble(AvroContext context) {
        AvroBenchmark.contentSerializeBenchmark(context, Contents.doubleVal());
    }

    @Benchmark
    public static void avroSerializeString(AvroContext context) {
        AvroBenchmark.contentSerializeBenchmark(context, Contents.stringVal());
    }

    @Benchmark
    public static void avroSerializeMixed(AvroContext context) {
        AvroBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }

}

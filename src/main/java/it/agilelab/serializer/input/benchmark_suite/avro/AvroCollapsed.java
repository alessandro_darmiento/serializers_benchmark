package it.agilelab.serializer.input.benchmark_suite.avro;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.AvroBenchmark;
import it.agilelab.serializer.procedures.ColferBenchmark;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

public class AvroCollapsed {

    @Benchmark
    public static void avroSerializeDigest(AvroContext context) {
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.low());
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.med());
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.high());
        AvroBenchmark.sizeSerializeBenchmark(context, Levels.extreme());

        AvroBenchmark.contentSerializeBenchmark(context, Contents.emptyVal());
        AvroBenchmark.contentSerializeBenchmark(context, Contents.longVal());
        AvroBenchmark.contentSerializeBenchmark(context, Contents.doubleVal());
        AvroBenchmark.contentSerializeBenchmark(context, Contents.stringVal());
        AvroBenchmark.contentSerializeBenchmark(context, Contents.mixedVal());
    }

    @Benchmark
    public static void avroDeserializeDigest(AvroContext context) {
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.low());
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.med());
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.high());
        AvroBenchmark.sizeDeserializeBenchmark(context, Levels.extreme());

        AvroBenchmark.contentDeserializeBenchmark(context, Contents.emptyVal());
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.longVal());
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.doubleVal());
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.stringVal());
        AvroBenchmark.contentDeserializeBenchmark(context, Contents.mixedVal());
    }
}

package it.agilelab.serializer.input.benchmark_suite.avro;

import it.agilelab.serializer.utils.SwallowerOutputStream;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

@State(Scope.Thread)
public class AvroContext {

    private BinaryEncoder encoder;
    private BinaryDecoder decoder;
    private Blackhole blackhole;

    @Setup
    public void init(Blackhole bh) {
        blackhole = bh;
        encoder = EncoderFactory.get().directBinaryEncoder(new SwallowerOutputStream(blackhole), null);
        decoder = DecoderFactory.get().binaryDecoder(new byte[1000000], null);
    }

    public BinaryEncoder getEncoder() {
        return encoder;
    }

    public BinaryDecoder getDecoder() {
        return decoder;
    }

    public Blackhole getBlackhole() {
        return blackhole;
    }
}

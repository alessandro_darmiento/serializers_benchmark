package it.agilelab.serializer.input.benchmark_suite.json;

import it.agilelab.serializer.input.Contents;
import it.agilelab.serializer.input.Levels;
import it.agilelab.serializer.procedures.JsonBenchmark;
import it.agilelab.serializer.procedures.ProtoBenchmark;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

/**
 * Execute tests with low granularity level
 */
public class JsonCollapsed {

    @Benchmark
    public static void jsonSerializeDigest(Blackhole blackhole) {
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.low());
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.med());
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.high());
        JsonBenchmark.sizeSerializeBenchmark(blackhole, Levels.extreme());

        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.emptyVal());
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.longVal());
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.doubleVal());
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.stringVal());
        JsonBenchmark.contentSerializeBenchmark(blackhole, Contents.mixedVal());
    }

    @Benchmark
    public static void jsonDeserializeDigest(Blackhole blackhole) {
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.low());
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.med());
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.high());
        JsonBenchmark.sizeDeserializeBenchmark(blackhole, Levels.extreme());

        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.emptyVal());
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.longVal());
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.doubleVal());
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.stringVal());
        JsonBenchmark.contentDeserializeBenchmark(blackhole, Contents.mixedVal());
    }

}

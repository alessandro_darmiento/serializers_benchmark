package it.agilelab.serializer.input;

import java.security.SecureRandom;
import java.util.Locale;


/**
 * Custom Java Random extension with built in secureRandom and String generation
 */
public class RandomUtil extends SecureRandom {

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final char[] symbols = (upper + lower + digits).toCharArray();

    public static final int DEF_SIZE = 6;

    private char[] buf;

    public RandomUtil() {
        super();
        this.buf = new char[DEF_SIZE];
    }

    /**
     * Return a random Long defined on at least 63 bits
     *
     * @return
     */
    public Long nextBigLong() {
        return Long.MAX_VALUE / 2 + Math.abs(nextLong() / 2);
    }

    /**
     * Return a random Double defined on at least 63 bits
     *
     * @return
     */
    public Double nextBigDouble() {
        return Double.MAX_VALUE / 2 + nextDouble() / 2;
    }


    /**
     * Generate a random string.
     */
    public String nextString(int length) {
        makeBuf(length);
        for (int idx = 0; idx < length; ++idx)
            buf[idx] = symbols[nextInt(symbols.length)];
        return new String(buf).substring(0, length);
    }

    /**
     * Generate a random string.
     */
    public String nextString() {
        return nextString(DEF_SIZE);
    }

    private void makeBuf(int length) {
        if (length < 1) throw new IllegalArgumentException();
        if (buf.length < length) {
            this.buf = new char[length];
        }
    }

}